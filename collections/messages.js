Messages = new Mongo.Collection("messages");
Messages.allow({
  insert: function (userId, message) {
    return userId && message.createdBy === userId && message.text.trim().length && message.groupId;
  }
});


if (Meteor.isServer && Meteor.settings.development) {
  Meteor.startup(function () {
    Messages.remove({});
  });
}

