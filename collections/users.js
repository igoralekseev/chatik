Meteor.users.allow({
  update: function (userId, user) {
    return user._id == userId;
  }
});

Meteor.users.after.insert(function(userId, doc) {
  fetchGroups(doc._id);
});

if (Meteor.isServer && Meteor.settings.development) {
  Meteor.startup(function () {
    Meteor.users.remove({});
  });
}