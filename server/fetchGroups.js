var vk = new VK({
   'appID'     : Meteor.settings.vk.APP_ID,
   'appSecret' : Meteor.settings.vk.APP_SECRET,
   'language'  : 'ru',

   'mode': 'oauth',
   'version': '5.3'
});

fetchGroups = function(userId) {
  var user = Meteor.users.findOne({ _id: userId });
  
  vk.setToken({ token: user.services.vk.accessToken });
    vk.request('getGroups', {
      extended: 1,
      fields: 'members_count',
      count: 100
    }, Meteor.bindEnvironment(function(results) {

      if (!results.response || !results.response.items)
        return;
      
      results.response.items.forEach(function(item, i) {
        item.vkOrder = i;
      });

      Meteor.users.update({ _id: userId }, { $set: { 
        groups: _.indexBy(results.response.items, 'id')
      }});
    }));
};