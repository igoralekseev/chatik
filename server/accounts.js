ServiceConfiguration.configurations.remove({
    service: 'vk'
});

ServiceConfiguration.configurations.insert({
    service: 'vk',
    appId:   Meteor.settings.vk.APP_ID,      
    secret:  Meteor.settings.vk.APP_SECRET 
});

Accounts.onCreateUser(function(options, user) {
  if (options.profile)
    user.profile = options.profile;
    user.profile.photo = user.services.vk.photo;
    
  return user;
});

