EMOJI = {
  check: function() {
    if (this.supported !== undefined)
      return this.supported;

    var node = document.createElement('canvas');
    if (!node.getContext) 
      return (this.supported = false);

    var ctx = node.getContext('2d');
    ctx.fillStyle = '#f00';
    ctx.textBaseline = 'top';
    ctx.font = '32px Arial';
    ctx.fillText('\ud83d\udc28', 0, 0); // U+1F428 KOALA

    var pixelRatio = window.devicePixelRatio || 1;
    var offset = 12 * pixelRatio;  

    return (this.supported = ctx.getImageData(offset, offset, 1, 1).data[0] !== 0);
  },

  // replace emoji with text - for ex. [smile] 
  replace: function(text, delimiterBefore, delimimterAfter) {
    if (!delimiterBefore) delimiterBefore = '';
    if (!delimimterAfter) delimimterAfter = delimiterBefore;

    return text.replace(/\ud83d[\ude00-\ude4f]/g, function(em) {
      return EMOJI_TEXT[em];
    });
  }
};